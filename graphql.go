package graphql

import (
	"encoding/json"
	"log"

	"github.com/gorilla/websocket"
	"github.com/graphql-go/graphql"
)

type commandType int

type Graphql struct {
	Query          rootType
	Mutation       rootType
	Subscription   rootType
	ShowPlayground bool
}

const (
	Query            commandType = iota
	Mutation         commandType = iota
	Subscription     commandType = iota
	QueryAndMutation commandType = iota
	All              commandType = iota
)

// Default Create an instance of Graphql, includes Query and Mutation RootType
func Default() *Graphql {
	return New(QueryAndMutation)
}

// New Create an instance for CommandType and add mapping RootType
func New(commandType commandType) *Graphql {
	ql := new(Graphql)
	switch commandType {
	case Query:
		ql.Query.new("Query", "Related commands to search and get data")
	case Mutation:
		ql.Mutation.new("Mutation", "Related commands to create, patch and delete data")
	case Subscription:
		ql.Subscription.new("Subscription", "The related command for subscription")
	case All:
		ql.Subscription.new("Subscription", "The related command for subscription")
		fallthrough
	case QueryAndMutation:
		ql.Query.new("Query", "Related commands to search and get data")
		ql.Mutation.new("Mutation", "Related commands to create, patch and delete data")
	}
	return ql
}

// CheckSubscription subscriptionName: Observe Schema name check:Validate Graphql Variables
func (g *Graphql) CheckSubscription(subscriptionName string, check map[string]interface{}) bool {
	var b bool
	subscribers.Range(func(key, value interface{}) bool {
		msg, ok := value.(*connectionACKMessage)
		if !ok {
			return true
		}
		if msg.Payload.OperationName == subscriptionName {
			opt := g.createParams(&msg.Payload)
			for s, i := range opt.VariableValues {
				if check[s] != nil {
					if check[s] != i {
						return true
					}
				}
			}
			res := graphql.Do(opt)

			message, _ := json.Marshal(map[string]interface{}{
				"id":      msg.OperationID,
				"type":    "data",
				"payload": res,
			})
			if err := msg.Conn.WriteMessage(websocket.TextMessage, message); err != nil {
				if err == websocket.ErrCloseSent {
					subscribers.Delete(key)
					return true
				}
				log.Printf("failed to write to ws connection: %v", err)
				return true
			}
			b = true
			return true
		}
		return true
	})
	return b
}
