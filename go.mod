module gitlab.com/champinfo/go/champ-iris-graphql

go 1.14

require (
	github.com/gorilla/websocket v1.5.0
	github.com/graphql-go/graphql v0.8.0
	github.com/kataras/iris/v12 v12.1.8
	gitlab.com/champinfo/go/champiris v0.2.17
)
