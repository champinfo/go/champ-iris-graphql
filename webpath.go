package graphql

import (
	"encoding/json"
	"fmt"
	"html/template"
	"strings"
	"sync"

	"github.com/gorilla/websocket"
	"github.com/graphql-go/graphql"
	"github.com/kataras/iris/v12"
)

var subscribers sync.Map

type connectionACKMessage struct {
	*websocket.Conn
	OperationID string         `json:"id,omitempty"`
	Type        string         `json:"type"`
	Payload     requestOptions `json:"payload,omitempty"`
}

// PostQuery Graphql API
// Entry point POST http://Host:Port/Party/query
// Base on iris mvc structure
func (g *Graphql) PostQuery(ctx iris.Context) {
	if !g.ShowPlayground {
		graphql.SchemaMetaFieldDef.Resolve = func(p graphql.ResolveParams) (interface{}, error) {
			return nil, nil
		}
		graphql.TypeMetaFieldDef.Resolve = func(p graphql.ResolveParams) (interface{}, error) {
			return nil, nil
		}
	}
	params := g.createParams(g.getRequestOptions(ctx))
	res := graphql.Do(params)
	rJSON, _ := json.Marshal(res)
	ctx.Write(rJSON)
}

// GetSubscription Graphql Apollo Server Subscriptions websocket
// Entry point GET ws://Host:Port/Party/subscription
// Base on iris mvc structure
func (g *Graphql) GetSubscription(ctx iris.Context) {
	conn, err := upgrader.Upgrade(ctx.ResponseWriter(), ctx.Request(), nil)
	if err != nil {
		fmt.Printf("failed to do websocket upgrade: %v", err)
		return
	}
	connectionACK, err := json.Marshal(map[string]string{
		"type": "connection_ack",
	})
	if err != nil {
		fmt.Printf("failed to marshal ws connection ack: %v", err)
	}
	if err := conn.WriteMessage(websocket.TextMessage, connectionACK); err != nil {
		fmt.Printf("failed to write to ws connection: %v", err)
		return
	}
	var msg connectionACKMessage
	msg.Conn = conn
	go func() {
		for {
			_, p, err := msg.Conn.ReadMessage()
			if websocket.IsCloseError(err, websocket.CloseGoingAway) {
				return
			}
			if err != nil {
				fmt.Printf("failed to read websocket message: %v", err)
				return
			}
			if err := json.Unmarshal(p, &msg); err != nil {
				fmt.Printf("failed to unmarshal: %v", err)
				return
			}
			if msg.Type == "start" {
				length := 0
				subscribers.Range(func(key, value interface{}) bool {
					length++
					return true
				})
				if msg.Payload.UUID != "" {
					subscribers.Store(msg.Payload.UUID, &msg)
				} else {
					subscribers.Store(ctx.GetHeader("Sec-Websocket-Key"), &msg)
				}
			}
			if msg.Type == "stop" {
				if msg.Payload.UUID != "" {
					subscribers.Delete(msg.Payload.UUID)
				} else {
					subscribers.Delete(ctx.GetHeader("Sec-Websocket-Key"))
				}
			}
		}
	}()
}

// GetPg Graphql Playground static webpage
// Entry point GET http://Host:Port/Party/pg
func (g *Graphql) GetPg(ctx iris.Context) {
	if !g.ShowPlayground {
		ctx.StatusCode(404)
		return
	}
	t := template.New("Playground")
	te, _ := t.Parse(html)
	endpoint := strings.Replace(ctx.RequestPath(true), "pg", "query", 1)
	subEndpoint := strings.Replace(endpoint, "query", "subscription", 1)
	_ = te.ExecuteTemplate(ctx.ResponseWriter(), "index", struct {
		Endpoint             string
		SubscriptionEndpoint string
	}{
		Endpoint:             endpoint,
		SubscriptionEndpoint: subEndpoint,
	})
}
