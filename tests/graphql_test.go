package tests

import (
	"testing"

	"github.com/kataras/iris/v12/mvc"
	graphql "gitlab.com/champinfo/go/champ-iris-graphql"
	"gitlab.com/champinfo/go/champiris"
)

var Graph *graphql.Graphql

func init() {
	Graph = graphql.New(graphql.All)
	Graph.ShowPlayground = true
}

func TestGraphql(t *testing.T) {
	var service champiris.Service
	_ = service.New(&champiris.NetConfig{
		Port: "8080",
	})
	service.App.Logger().SetLevel("debug")
	service.AddRoute("/service/v1", func(m *mvc.Application) {
		m.Handle(Graph)
	})
	addSchema()
	err := service.Run()
	if err != nil {
		t.Error(err)
	}
}
